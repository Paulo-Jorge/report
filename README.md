## Reports R&D

Website created for data collection for statistical treatment, used to creat annual reports of Research Centers. In Portugal, Research Centers have to report annually to FCT, DGEEC, (etc.) with the scientific output and previsions for the following years. For that we need to get reports from each researcher (+/- 150 in total where I work). To make that easier I created a website with a form to collect the data (I was asked to make it that registration is not mandatory, so I created a different kind of system for an easier login, optional only in case of edit), a system to export the information to other formats (PDF, WORD, EXCEL, etc.), and a private page for the data manipulation and treatment (allows filtering by many different criteria, and formats everything automatically for copy paste in the final report format). Usually I avoid sharing source-code of recent stuff or apps that I have still active (for security reasons, it is easier to exploit if one knows the raw code), but at the moment I am migrating to a better system (integrated this logic with a CV system and a better report generator embedded in public website). For that reason I am releasing the source-code of the full app in open-source. Hope it is useful for other research centers (it is easy to adapt). PS: 10 years ago they did this reports manually only using .doc documents and e-mails and took 2 months, now in 10 minuts is done - informatics really make life easier ☺

---

## Want to run this?

This website was programmed with the Python (version 2.7) framework Web2py (use the most recent one) + SQLite for database. You'll need Web2py to run this: just copy this app to the "applications" folder and it will run. It is easy to run a local instance with the Web2py binary package ("Batteries Included" with a mini server). For professional and public web you'll need to configure a server to run Python2.7/Web2py (my favourite stack: Nginx + UWSGI)

For more information check the Web2py website: [http://www.web2py.com/](http://www.web2py.com/)

Note: to acess the private page with the processed user data the relative url path is: http://www.xx.com**/report/default/ver**

You can test the website in this temporary instance (temp server with PythonAnywhere, can go off anytime):
- Entry page: [http://toaki.pythonanywhere.com/report](http://toaki.pythonanywhere.com/report)
- Private page: [http://toaki.pythonanywhere.com/report/default/ver](http://toaki.pythonanywhere.com/report/default/ver)

---

## Contact me:

[http://www.paulojorgepm.net](http://www.paulojorgepm.net)