# coding: utf8

YEAR = '2018'

#List with Tuple of title + db field name to used in the pdf and rtf converters
#List template: 'Title to show':'db_name' note: if the field db name is empty it will be trated like a title, not a textarea
#I use Tuples inside a list instead of a dict because they loose order

fields_personal = [('Nome:','nome'),
('E-mail:','email'),
('Categoria Académica:','categoria'),
('Grau:','grau')]

fields_data = [
('1. Projetos',''),
('1.1. Projeto(s) não financiados (indicar título, data de início e supervisor/coordenador):','f1_1'),
('1.2. Projeto(s) financiado(s) pela FCT ou outras entidades nacionais (indicar título, data de início e investigador responsável):','f1_2'),
('2. Internacionalização',''),
('2.1. Participação em projetos ou redes internacionais (indicar título, instituição responsável, URL, financiamento e data de início e fim, se aplicável):','f2_1'),
('3. Publicações',''),
('Nota importante: As referências das publicações devem ser indicadas em formato APA, com indicação do URL (referente ao depósito em repositório / link da revista / DOI / indexação / e-Book)',''),
('3.1. Livros (indicar se é monografia, volume editado, etc.):','f3_1'),
('3.2. Capítulos de livros (indicar se for peer review)',''),
('3.2.1. Capítulos de livros nacionais:','f3_2_1'),
('3.2.2. Capítulos de livros internacionais:','f3_2_2'),
('3.3. Artigos em livros de atas:','f3_3'),
('3.4. Publicações em revistas (indicar se for peer review)',''),
('3.4.1. Artigos em revistas nacionais:','f3_4_1'),
('3.4.2. Artigos em revistas internacionais:','f3_4_2'),
('3.5 Outras publicações (não indicar publicações no prelo):','f3_5'),
('4. Comunicações',''),
('4.1. Eventos nacionais (indicar se por convite ou submissão de resumo):','f4_1'),
('4.2. Eventos internacionais (indicar se por convite ou submissão de resumo):','f4_2'),
('5. Organização de eventos (Indicar título, data, local e URL do evento)','f5'),
('6. Orientação de teses',''),
('6.1. Doutoramentos (indicar título, nome do candidato(a) e curso; dividir entre teses em desenvolvimento e terminadas - indicar a data de defesa se aplicável):','f6_1'),
('6.2. Mestrados (indicar título, nome do candidato(a) e curso; dividir entre teses em desenvolvimento e terminadas - indicar a data de defesa se aplicável):','f6_2'),
('6.3. Pós-Doutoramentos em curso (título, datas e nome do investigador(a)):','f6_3'),
('7. Participação em equipas editoriais e comissões científicas de revistas (indicar cargo, revista e URL)','f7'),
('8. Atividades previstas para o próximo ano',''),
('8.1. Atividades no âmbito de projetos (p.e. projetos a submeter a financiamento):','f8_1'),
('8.2. Publicações (indicar o título e se está em preparação, em fase de avaliação ou no prelo):','f8_2'),
('8.3. Comunicações em eventos (indicar URL do evento, título (provisório) da comunicação):','f8_3'),
('8.4. Organização de eventos (indicar tipo de evento, local, equipa organizadora, financiamento):','f8_4'),
('9. Cargos de Gestão no ILCH ou na Universidade','f9'),
('10. Júris',''),
('10.1. Júris de Provas (Mestrado, Doutoramento e Agregação):','f10_1'),
('10.2. Outros painéis de avaliação:','f10_2'),
('11. Participação em Órgãos Sociais de Associações ou Sociedades Científicas','f11'),
('12. Outras atividades de extensão','f12')
]
