# -*- coding: utf-8 -*-

db = DAL('sqlite://storage.sqlite')

## by default give a view/generic.extension to all actions from localhost
## none otherwise. a pattern can be 'controller/function.extension'
response.generic_patterns = ['*'] if request.is_local else []
## (optional) optimize handling of static files
# response.optimize_css = 'concat,minify,inline'
# response.optimize_js = 'concat,minify,inline'

#########################################################################
## Here is sample code if you need for
## - email capabilities
## - authentication (registration, login, logout, ... )
## - authorization (role based authorization)
## - services (xml, csv, json, xmlrpc, jsonrpc, amf, rss)
## - old style crud actions
## (more options discussed in gluon/tools.py)
#########################################################################

from gluon.tools import Auth, Crud, Service, PluginManager, prettydate
auth = Auth(db)
crud, service, plugins = Crud(db), Service(), PluginManager()

## create all tables needed by auth if not custom tables
auth.define_tables(username=False, signature=False)

## configure email
mail = auth.settings.mailer
mail.settings.server = 'mail.uminho.pt:25'
mail.settings.sender = 'xxxx@xxx.xxx.pt'
mail.settings.login = 'xxx:xxx'

## configure auth policy
#auth.settings.registration_requires_verification = False
#auth.settings.registration_requires_approval = False
#auth.settings.reset_password_requires_verification = True

## if you need to use OpenID, Facebook, MySpace, Twitter, Linkedin, etc.
## register with janrain.com, write your domain:api_key in private/janrain.key
from gluon.contrib.login_methods.rpx_account import use_janrain
use_janrain(auth, filename='private/janrain.key')

#########################################################################
## Define your tables below (or better in another model file) for example
##
## >>> db.define_table('mytable',Field('myfield','string'))
##
## Fields can be 'string','text','password','integer','double','boolean'
##       'date','time','datetime','blob','upload', 'reference TABLENAME'
## There is an implicit 'id integer autoincrement' field
## Consult manual for more options, validators, etc.
##
## More API examples for controllers:
##
## >>> db.mytable.insert(myfield='value')
## >>> rows=db(db.mytable.myfield=='value').select(db.mytable.ALL)
## >>> for row in rows: print row.id, row.myfield
#########################################################################



db.define_table('relatorio',
Field('nome','string', requires=IS_NOT_EMPTY(), label="Nome"),
Field('email','string', requires=IS_NOT_EMPTY(), label="E-mail"),
Field('number','string', requires=IS_NOT_EMPTY(), label="Nº"),
Field('department','string', requires=IS_IN_SET(['Nenhum', 'DEA', 'DEGE', 'DEINA', 'DEPL', 'DER', 'DFIL', 'DM']), label="Departamento"),
Field('centre','string', requires=IS_IN_SET(['Nenhum', 'CEHUM', 'CEPS']), label="Centro"),
Field('categoria','string', requires=IS_IN_SET(['Assistente estagiário', 'Leitor', 'Assistente', 'Prof. Auxiliar convidado', 'Prof. Auxiliar sem agregação', 'Prof. Auxiliar com agregação', 'Prof. Associado sem agregação', 'Prof. Associado com agregação', 'Prof. Catedrático', 'Investigador (com bolsa)', 'Investigador (sem bolsa)', 'Investigador contratado', 'Outro']), label="Categoria"),
Field('grau','string', requires=IS_IN_SET(['Licenciado', 'Mestre', 'Doutor', 'Agregação']), label="Grau"),
#Field('grupo','string', requires=IS_IN_SET(['EHUM2M', 'GALABRA-UMINHO', 'GAPS', 'GELA', 'GHD', 'GRUPO2I', 'NETCULT', 'LTE', 'PLP', 'PRADIC', 'GIEP', multiple=True])),
Field('f1_1','text', label="1.1. Projeto(s) não financiados (indicar título, data de início e supervisor/coordenador)"),
Field('f1_2','text', label="1.2. Projeto(s) financiado(s) pela FCT ou outras entidades nacionais (indicar título, data de início e investigador responsável)"),
Field('f2_1','text', label="2.1. Participação em projetos ou redes internacionais (indicar título, instituição responsável, URL, financiamento e data de início e fim, se aplicável)"),
Field('f3_1','text', label="3.1. Livros (indicar se é monografia, volume editado, etc.)"),
Field('f3_2_1','text', label="3.2.1. Capítulos de livros nacionais"),
Field('f3_2_2','text', label="3.2.2. Capítulos de livros internacionais"),
Field('f3_3','text', label="3.3. Artigos em livros de atas"),
Field('f3_4_1','text', label="3.4.1. Artigos em revistas nacionais"),
Field('f3_4_2','text', label="3.4.2. Artigos em revistas internacionais"),
Field('f3_5','text', label="3.5 Outras publicações (não indicar publicações no prelo)"),
Field('f4_1','text', label="4.1. Eventos nacionais (indicar se por convite ou submissão de resumo)"),
Field('f4_2','text', label="4.2. Eventos internacionais (indicar se por convite ou submissão de resumo)"),
Field('f5','text', label="5. Organização de eventos (Indicar título, data, local e URL do evento)"),
Field('f6_1','text', label="6.1. Doutoramentos (indicar título, nome do candidato(a) e curso; dividir entre teses em desenvolvimento e terminadas - indicar a data de defesa se aplicável)"),
Field('f6_2','text', label="6.2. Mestrados (indicar título, nome do candidato(a) e curso; dividir entre teses em desenvolvimento e terminadas - indicar a data de defesa se aplicável)"),
Field('f6_3','text', label="6.3. Pós-Doutoramentos em curso (título, datas e nome do investigador(a))"),
Field('f7','text', label="7. Participação em equipas editoriais e comissões científicas de revistas (indicar cargo, revista e URL)"),
Field('f8_1','text', label="8.1. Atividades no âmbito de projetos (p.e. projetos a submeter a financiamento)"),
Field('f8_2','text', label="8.2. Publicações (indicar o título e se está em preparação, em fase de avaliação ou no prelo)"),
Field('f8_3','text', label="8.3. Comunicações em eventos (indicar URL do evento, título (provisório) da comunicação)"),
Field('f8_4','text', label="8.4. Organização de eventos (indicar tipo de evento, local, equipa organizadora, financiamento)"),

Field('f9','text', label="9. Cargos de Gestão no ILCH ou na Universidade"),
Field('f10_1','text', label="10.1. Júris de Provas (Mestrado, Doutoramento e Agregação)"),
Field('f10_2','text', label="10.2. Outros painéis de avaliação"),
Field('f11','text', label="11. Participação em Órgãos Sociais de Associações ou Sociedades Científicas"),
Field('f12','text', label="12. Outras atividades de extensão"),

Field('data','datetime', default=request.now, readable=False, writable=False),
Field('password','string', readable=False, writable=False),
Field('private_link','string', readable=False, writable=False),
Field('submited','string', readable=False, writable=False, requires=IS_IN_SET(['yes', 'no'])),
Field('ip_client','string', readable=False, writable=False)
)

## after defining tables, uncomment below to enable auditing
# auth.enable_record_versioning(db)
