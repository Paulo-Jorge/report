# -*- coding: utf-8 -*-
# this file is released under public domain and you can use without limitations

#########################################################################
## This is a samples controller
## - index is the default action of any application
## - user is required for authentication and authorization
## - download is for downloading files uploaded in the db (does streaming)
## - call exposes all registered services (none by default)
#########################################################################
import string
import random
import re

def is_number(x): #determina se na string do arg existe um número
    try:
        float(x)
        return True
    except:
        return False

def is_letters_digits(x): #determina se contém apenas letras e números. Filtra todo o tipo de símbolos e palavras acentuadas.
    if re.match("^[0-9 a-z A-Z]+$", x):
        return True
    else:
        return False

def index():
    #redirect(URL('default', 'closed'))
    
    return dict()

def closed():
    return dict()

def new_extra(form):
    private_link = ''.join(random.SystemRandom().choice(string.digits) for _ in range(10))
    form.vars.private_link = str(private_link)

    try:
        #get last id, to know current one, since form.varis.id was not created yet, only after validation
        last_id = db(db.relatorio.id > 1).select(db.relatorio.id, orderby=~db.relatorio.id, limitby=(0,1)).first()

        #add current id to private_link, just in case to avoid if the random creats a duplicated one with the same password.
        #Just to make sure there are no 2 the same
        form.vars.private_link = str(last_id["id"] + 1) + form.vars.private_link + str(last_id["id"] + 1)
    except:
        form.vars.private_link = "1" + form.vars.private_link + "1"

    password = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits + string.ascii_lowercase) for _ in range(10))
    form.vars.password = password

    form.vars.submited = "no"
    form.vars.ip_client = request.client

    session.code = form.vars.private_link
    session.password = form.vars.password

def new():
    
    
    #Para desligar tirar comentário
    #redirect(URL('default', 'index'))
    #redirect(URL('default', 'closed'))


    form=SQLFORM(db.relatorio)

    if form.process(onvalidation=new_extra).accepted:
        texto = 'E-mail automático: Relatório '+ YEAR +' submetido com sucesso!\r\n\r\nDados de login para aceder ao seu relatório:\r\n\r\n' + 'Identificador: ' + form.vars.private_link + '\r\n' + 'Password: ' + form.vars.password + '\r\n\r\n' + 'Até ao dia de encerramento das submissões poderá editar o seu relatório quantas vezes necessárias.\r\n\r\nWebsite da paltaforma:\r\nhttp://'+ YEAR +'\r\n\r\nCumprimentos'
        try:
            mail.send(to=form.vars.email,
            subject='Relatório '+ YEAR +' - Dados de Acesso',
            #If reply_to is omitted, then mail.settings.sender is used
            #reply_to=[form.vars.email],
            message=texto )
            session.estado_email = estado_email = 'yes'
        except:
            session.estado_email = estado_email = 'no'

        session.flash = 'Relatório Enviado com Sucesso'
        redirect(URL('default', 'edit'))
    elif form.errors:
        response.flash = "Erros no formulário!"

    return dict(form=form)

def logout():
    session.code = ""
    session.password = ""
    redirect(URL('default', 'index'))

def edit():

    #Para desligar tirar comentário
    #redirect(URL('default', 'index'))
    #redirect(URL('default', 'closed'))

    login = "" #no, yes, invalid

    if is_number(request.post_vars["code"]) and is_letters_digits(request.post_vars["password"]):
        session.code = request.post_vars["code"]
        session.password = request.post_vars["password"]

    if session.code and session.password:
        try:
            contents = db(db.relatorio.private_link == session.code).select().first()
            password = contents["password"]
        except:
            password = None
        if password == session.password:
            login = "yes"
            contents = db(db.relatorio.private_link == session.code).select().first()
            record = db.relatorio(contents["id"])
            form = SQLFORM(db.relatorio, record)
            if form.process().accepted:
                #If we don't update contents, it will load the old ones again. We could also redirect to refresh.
                contents = db(db.relatorio.private_link == session.code).select().first()
                response.flash = 'Relatório Enviado com Sucesso'
                texto = 'E-mail automático: Relatório '+ YEAR +' submetido com sucesso!\r\n\r\nDados de login para aceder ao seu relatório:\r\n\r\n' + 'Identificador: ' + str(contents["private_link"]) + '\r\n' + 'Password: ' + str(contents["password"]) + '\r\n\r\n' + 'Até ao dia de encerramento das submissões poderá editar o seu relatório quantas vezes necessárias.\r\n\r\nWebsite da paltaforma:\r\nhttp://'+ YEAR +'\r\n\r\nCumprimentos'
                try:
                    mail.send(to=contents["email"],
                    subject='Relatório '+ YEAR +' - Dados de Acesso',
                    #If reply_to is omitted, then mail.settings.sender is used
                    #reply_to=[form.vars.email],
                    message=texto )
                    session.estado_email = estado_email = 'yes'
                except:
                    session.estado_email = estado_email = 'no'
            elif form.errors:
                #when we get error, if we don't update the contents, it will go back to the original, loosing the new changes
                for key in contents.iterkeys():
                    if key != "id":
                        contents[key] = form.vars[key]
                response.flash = "Erros no formulário!"

            return dict(login=login, form=form, contents=contents)

        else:
            login = "no"

    else:
        login = "no"

    return dict(login=login)


def export_rtf():
    try:
        relatorio = db(db.relatorio.private_link == session.code).select().first()
        password = relatorio["password"]
    except:
        password = None

    if session.password and password == session.password:
        import gluon.contrib.pyrtf as rtf
        import rtfunicode
        #had problems with encodings, chinese and accented letters to rtf, found this rtfunicode script in:
        #http://www.zopatista.com/python/2012/06/06/rtf-and-unicode/
        #github in: https://github.com/mjpieters/rtfunicode

        doc=rtf.Document()
        ss = doc.StyleSheet
        section=rtf.Section()
        doc.Sections.append(section)

        h1 = rtf.Paragraph( ss.ParagraphStyles.Heading1 )

        h1.append( str("Relatório "+YEAR).decode("utf-8").encode('rtfunicode') )
        section.append(h1)

        p = rtf.Paragraph( ss.ParagraphStyles.Normal )
        p.append( "" )
        section.append( p )

        h2 = rtf.Paragraph( ss.ParagraphStyles.Heading2 )
        h2.append( "Dados Pessoais:".decode("utf-8").encode('rtfunicode') )
        section.append(h2)

        #for key in fields_personal:
        #for key, value in fields_personal.iteritems():
        for t in fields_personal:
            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( rtf.TEXT( str(t[0]+" ").decode("utf-8").encode('rtfunicode') ),
                  relatorio[t[1]].decode("utf-8").encode('rtfunicode'))
            section.append( p )

        p = rtf.Paragraph( ss.ParagraphStyles.Normal )
        p.append( "" )
        section.append( p )

        #Note: \n gives problems in the rtf, weird symbols, thats why I replace. But why \n? Must be a rtfunicode bug, must be confusing \n with a unicode char and trying to convert. I think it is rtfunicode, encodes \n to something else.
        #PS NEW: instead of replacing \n, I replaced the bad utf8 char ( \u10 - found it opening the rtf in txt mode) after all the convertings:
        for t in fields_data:
            if t[1] == '':
                h2 = rtf.Paragraph( ss.ParagraphStyles.Heading2 )
                h2.append( t[0].decode("utf-8").encode('rtfunicode') )
                section.append(h2)
            else:
                p = rtf.Paragraph( ss.ParagraphStyles.Normal )
                p.append( t[0].decode('utf-8').encode('rtfunicode'))
                section.append( p )

                p = rtf.Paragraph( ss.ParagraphStyles.Normal )
                #p.append( relatorio[t[1]].replace("\r\n","\r").decode('utf-8').encode('rtfunicode'))
                p.append( relatorio[t[1]].decode('utf-8').encode('rtfunicode').replace('\u10?',''))
                section.append( p )

                p = rtf.Paragraph( ss.ParagraphStyles.Normal )
                p.append( "" )
                section.append( p )

                #p = rtf.Paragraph( ss.ParagraphStyles.Normal )
                #p.append( rtf.TEXT( t[0]).decode("utf-8").encode('rtfunicode') ),
                #      relatorio[t[1]].decode("utf-8").encode('rtfunicode'))
                #section.append( p )

        '''
        p = rtf.Paragraph( ss.ParagraphStyles.Normal )
        p.append( "" )
        section.append( p )

        h2 = rtf.Paragraph( ss.ParagraphStyles.Heading2 )
        h2.append( "Dados Pessoais:".decode("utf-8").encode('rtfunicode') )
        section.append(h2)

        p = rtf.Paragraph( ss.ParagraphStyles.Normal )
        p.append( rtf.TEXT( "Nome: ".decode("utf-8").encode('rtfunicode') ),
              relatorio["nome"].decode("utf-8").encode('rtfunicode'))
        section.append( p )

        p = rtf.Paragraph( ss.ParagraphStyles.Normal )
        p.append( "E-mail: ".decode("utf-8").encode('rtfunicode'),
              relatorio["email"].decode('utf-8').encode('rtfunicode'))
        section.append( p )

        p = rtf.Paragraph( ss.ParagraphStyles.Normal )
        p.append( "Categoria Académica: ".decode("utf-8").encode('rtfunicode'),
              relatorio["categoria"].decode('utf-8').encode('rtfunicode'))
        section.append( p )

        p = rtf.Paragraph( ss.ParagraphStyles.Normal )
        p.append( "Grau: ".decode("utf-8").encode('rtfunicode'),
              relatorio["grau"].decode('utf-8').encode('rtfunicode'))
        section.append( p )

        p = rtf.Paragraph( ss.ParagraphStyles.Normal )
        p.append( "Linha Temática: ".decode('utf-8').encode('rtfunicode'),
              relatorio["linha"].decode('utf-8').encode('rtfunicode'))
        section.append( p )

        p = rtf.Paragraph( ss.ParagraphStyles.Normal )
        p.append( "" )
        section.append( p )

        h2 = rtf.Paragraph( ss.ParagraphStyles.Heading2 )
        h2.append( "1. Projeto de investigação em curso:".decode("utf-8").encode('rtfunicode') )
        section.append(h2)

        p = rtf.Paragraph( ss.ParagraphStyles.Normal )
        p.append( "1. 1. Projeto de Mestrado, Doutoramento, Pós-Doutoramento (indicar título, data de início e supervisor):".decode('utf-8').encode('rtfunicode'))
        section.append( p )

        p = rtf.Paragraph( ss.ParagraphStyles.Normal )
        p.append( relatorio["f1_1"].replace("\n","").decode('utf-8').encode('rtfunicode'))
        #Note: it gives problems in the rtf. But why \n? My 1st guess was deleting all \r. I think it is rtfunicode, encodes \n to something else.
        section.append( p )

        p = rtf.Paragraph( ss.ParagraphStyles.Normal )
        p.append( "" )
        section.append( p )

        p = rtf.Paragraph( ss.ParagraphStyles.Normal )
        p.append( "1.2. Projeto(s) de investigação financiado(s) pela FCT ou outra entidade (indicar título, data de início e investigador responsável):".decode('utf-8').encode('rtfunicode'))
        section.append( p )

        p = rtf.Paragraph( ss.ParagraphStyles.Normal )
        p.append( relatorio["f1_2"].replace("\n","").decode('utf-8').encode('rtfunicode'))
        #Note: it gives problems in the rtf. But why \n? My 1st guess was deleting all \r. I think it is rtfunicode, encodes \n to something else.
        section.append( p )

        p = rtf.Paragraph( ss.ParagraphStyles.Normal )
        p.append( "" )
        section.append( p )

        h2 = rtf.Paragraph( ss.ParagraphStyles.Heading2 )
        h2.append( "2. Internacionalização".decode("utf-8").encode('rtfunicode') )
        section.append(h2)

        p = rtf.Paragraph( ss.ParagraphStyles.Normal )
        p.append( "2.1. Participação em Redes Internacionais e Europeias (indicar título, instituição responsável, financiamento e data de início e fim, se aplicável, e URL):".decode('utf-8').encode('rtfunicode'))
        section.append( p )

        p = rtf.Paragraph( ss.ParagraphStyles.Normal )
        p.append( relatorio["f2_1"].replace("\n","").decode('utf-8').encode('rtfunicode'))
        #Note: it gives problems in the rtf. But why \n? My 1st guess was deleting all \r. I think it is rtfunicode, encodes \n to something else.
        section.append( p )

        p = rtf.Paragraph( ss.ParagraphStyles.Normal )
        p.append( "" )
        section.append( p )

        h2 = rtf.Paragraph( ss.ParagraphStyles.Heading2 )
        h2.append( "3. Publicações de caráter científico".decode("utf-8").encode('rtfunicode') )
        section.append(h2)

        p = rtf.Paragraph( ss.ParagraphStyles.Normal )
        p.append( "Nota importante: As referências das publicações devem ser indicadas em formato APA, com indicação do URL (referente ao depósito em repositório / link da revista / DOI / indexação / e-Book).".decode('utf-8').encode('rtfunicode'))
        section.append( p )

        p = rtf.Paragraph( ss.ParagraphStyles.Normal )
        p.append( "" )
        section.append( p )

        p = rtf.Paragraph( ss.ParagraphStyles.Normal )
        p.append( "3.1. Livros ((indicar se é monografia, volume editado...):".decode('utf-8').encode('rtfunicode'))
        section.append( p )

        p = rtf.Paragraph( ss.ParagraphStyles.Normal )
        p.append( relatorio["f3_1"].replace("\n","").decode('utf-8').encode('rtfunicode'))
        #Note: it gives problems in the rtf. But why \n? My 1st guess was deleting all \r. I think it is rtfunicode, encodes \n to something else.
        section.append( p )

        p = rtf.Paragraph( ss.ParagraphStyles.Normal )
        p.append( "" )
        section.append( p )

        p = rtf.Paragraph( ss.ParagraphStyles.Normal )
        p.append( "3.2. Capítulos de livros (indicar se for peer review)".decode('utf-8').encode('rtfunicode'))
        section.append( p )

        p = rtf.Paragraph( ss.ParagraphStyles.Normal )
        p.append( "3.2.1 Capítulos de livros nacionais:".decode('utf-8').encode('rtfunicode'))
        section.append( p )

        p = rtf.Paragraph( ss.ParagraphStyles.Normal )
        p.append( relatorio["f3_2_1"].replace("\n","").decode('utf-8').encode('rtfunicode'))
        #Note: it gives problems in the rtf. But why \n? My 1st guess was deleting all \r. I think it is rtfunicode, encodes \n to something else.
        section.append( p )

        p = rtf.Paragraph( ss.ParagraphStyles.Normal )
        p.append( "" )
        section.append( p )

        p = rtf.Paragraph( ss.ParagraphStyles.Normal )
        p.append( "3.2.2 Capítulos de livros internacionais:".decode('utf-8').encode('rtfunicode'))
        section.append( p )

        p = rtf.Paragraph( ss.ParagraphStyles.Normal )
        p.append( relatorio["f3_2_2"].replace("\n","").decode('utf-8').encode('rtfunicode'))
        #Note: it gives problems in the rtf. But why \n? My 1st guess was deleting all \r. I think it is rtfunicode, encodes \n to something else.
        section.append( p )

        p = rtf.Paragraph( ss.ParagraphStyles.Normal )
        p.append( "" )
        section.append( p )

        p = rtf.Paragraph( ss.ParagraphStyles.Normal )
        p.append( "3.3. Artigos em livros de atas:".decode('utf-8').encode('rtfunicode'))
        section.append( p )

        p = rtf.Paragraph( ss.ParagraphStyles.Normal )
        p.append( relatorio["f3_3"].replace("\n","").decode('utf-8').encode('rtfunicode'))
        #Note: it gives problems in the rtf. But why \n? My 1st guess was deleting all \r. I think it is rtfunicode, encodes \n to something else.
        section.append( p )

        p = rtf.Paragraph( ss.ParagraphStyles.Normal )
        p.append( "" )
        section.append( p )

        p = rtf.Paragraph( ss.ParagraphStyles.Normal )
        p.append( "3.4. Publicações em Revistas (indicar se for peer review)".decode('utf-8').encode('rtfunicode'))
        section.append( p )

        p = rtf.Paragraph( ss.ParagraphStyles.Normal )
        p.append( "3.4.1. Artigos em revistas nacionais:".decode('utf-8').encode('rtfunicode'))
        section.append( p )

        p = rtf.Paragraph( ss.ParagraphStyles.Normal )
        p.append( relatorio["f3_4_1"].replace("\n","").decode('utf-8').encode('rtfunicode'))
        #Note: it gives problems in the rtf. But why \n? My 1st guess was deleting all \r. I think it is rtfunicode, encodes \n to something else.
        section.append( p )

        p = rtf.Paragraph( ss.ParagraphStyles.Normal )
        p.append( "" )
        section.append( p )

        p = rtf.Paragraph( ss.ParagraphStyles.Normal )
        p.append( "3.4.2 Artigos em revistas internacionais:".decode('utf-8').encode('rtfunicode'))
        section.append( p )

        p = rtf.Paragraph( ss.ParagraphStyles.Normal )
        p.append( relatorio["f3_4_2"].replace("\n","").decode('utf-8').encode('rtfunicode'))
        #Note: it gives problems in the rtf. But why \n? My 1st guess was deleting all \r. I think it is rtfunicode, encodes \n to something else.
        section.append( p )

        p = rtf.Paragraph( ss.ParagraphStyles.Normal )
        p.append( "" )
        section.append( p )

        p = rtf.Paragraph( ss.ParagraphStyles.Normal )
        p.append( "3.5 Outras publicações (não indicar publicações no prelo):".decode('utf-8').encode('rtfunicode'))
        section.append( p )

        p = rtf.Paragraph( ss.ParagraphStyles.Normal )
        p.append( relatorio["f3_5"].replace("\n","").decode('utf-8').encode('rtfunicode'))
        #Note: it gives problems in the rtf. But why \n? My 1st guess was deleting all \r. I think it is rtfunicode, encodes \n to something else.
        section.append( p )

        p = rtf.Paragraph( ss.ParagraphStyles.Normal )
        p.append( "" )
        section.append( p )

        h2 = rtf.Paragraph( ss.ParagraphStyles.Heading2 )
        h2.append( "4. Comunicações".decode("utf-8").encode('rtfunicode') )
        section.append(h2)

        p = rtf.Paragraph( ss.ParagraphStyles.Normal )
        p.append( "4.1. Encontros científicos nacionais (indicar se por convite ou submissão de resumo):".decode('utf-8').encode('rtfunicode'))
        section.append( p )

        p = rtf.Paragraph( ss.ParagraphStyles.Normal )
        p.append( relatorio["f4_1"].replace("\n","").decode('utf-8').encode('rtfunicode'))
        #Note: it gives problems in the rtf. But why \n? My 1st guess was deleting all \r. I think it is rtfunicode, encodes \n to something else.
        section.append( p )

        p = rtf.Paragraph( ss.ParagraphStyles.Normal )
        p.append( "" )
        section.append( p )

        p = rtf.Paragraph( ss.ParagraphStyles.Normal )
        p.append( "4.2. Encontros científicos internacionais (indicar se por convite ou submissão de resumo):".decode('utf-8').encode('rtfunicode'))
        section.append( p )

        p = rtf.Paragraph( ss.ParagraphStyles.Normal )
        p.append( relatorio["f4_2"].replace("\n","").decode('utf-8').encode('rtfunicode'))
        #Note: it gives problems in the rtf. But why \n? My 1st guess was deleting all \r. I think it is rtfunicode, encodes \n to something else.
        section.append( p )

        p = rtf.Paragraph( ss.ParagraphStyles.Normal )
        p.append( "" )
        section.append( p )

        h2 = rtf.Paragraph( ss.ParagraphStyles.Heading2 )
        h2.append( "5. Organização de Colóquios ou Seminários".decode("utf-8").encode('rtfunicode') )
        section.append(h2)

        p = rtf.Paragraph( ss.ParagraphStyles.Normal )
        p.append( "Indicar título, data, local, o URL do evento.".decode('utf-8').encode('rtfunicode'))
        section.append( p )

        p = rtf.Paragraph( ss.ParagraphStyles.Normal )
        p.append( relatorio["f5"].replace("\n","").decode('utf-8').encode('rtfunicode'))
        #Note: it gives problems in the rtf. But why \n? My 1st guess was deleting all \r. I think it is rtfunicode, encodes \n to something else.
        section.append( p )

        p = rtf.Paragraph( ss.ParagraphStyles.Normal )
        p.append( "" )
        section.append( p )

        h2 = rtf.Paragraph( ss.ParagraphStyles.Heading2 )
        h2.append( "6. Orientação de teses concluídas em 2018".decode("utf-8").encode('rtfunicode') )
        section.append(h2)

        p = rtf.Paragraph( ss.ParagraphStyles.Normal )
        p.append( "6.1. Doutoramentos (título, data de defesa e nome do candidato):".decode('utf-8').encode('rtfunicode'))
        section.append( p )

        p = rtf.Paragraph( ss.ParagraphStyles.Normal )
        p.append( relatorio["f6_1"].replace("\n","").decode('utf-8').encode('rtfunicode'))
        #Note: it gives problems in the rtf. But why \n? My 1st guess was deleting all \r. I think it is rtfunicode, encodes \n to something else.
        section.append( p )

        p = rtf.Paragraph( ss.ParagraphStyles.Normal )
        p.append( "" )
        section.append( p )

        p = rtf.Paragraph( ss.ParagraphStyles.Normal )
        p.append( "6.2. Mestrados (título, data de defesa e nome do candidato): ".decode('utf-8').encode('rtfunicode'))
        section.append( p )

        p = rtf.Paragraph( ss.ParagraphStyles.Normal )
        p.append( relatorio["f6_2"].replace("\n","").decode('utf-8').encode('rtfunicode'))
        #Note: it gives problems in the rtf. But why \n? My 1st guess was deleting all \r. I think it is rtfunicode, encodes \n to something else.
        section.append( p )

        p = rtf.Paragraph( ss.ParagraphStyles.Normal )
        p.append( "" )
        section.append( p )

        h2 = rtf.Paragraph( ss.ParagraphStyles.Heading2 )
        h2.append( "7. Participação em equipas editoriais e comissões científicas de revistas (indicar cargo, revista e URL)".decode("utf-8").encode('rtfunicode') )
        section.append(h2)

        p = rtf.Paragraph( ss.ParagraphStyles.Normal )
        p.append( relatorio["f7"].replace("\n","").decode('utf-8').encode('rtfunicode'))
        #Note: it gives problems in the rtf. But why \n? My 1st guess was deleting all \r. I think it is rtfunicode, encodes \n to something else.
        section.append( p )

        p = rtf.Paragraph( ss.ParagraphStyles.Normal )
        p.append( "" )
        section.append( p )

        h2 = rtf.Paragraph( ss.ParagraphStyles.Heading2 )
        h2.append( "8. Atividades previstas para 2019".decode("utf-8").encode('rtfunicode') )
        section.append(h2)

        p = rtf.Paragraph( ss.ParagraphStyles.Normal )
        p.append( "8.1. Atividades de investigação no âmbito de projetos (nomeadamente projetos a submeter a financiamento):".decode('utf-8').encode('rtfunicode'))
        section.append( p )

        p = rtf.Paragraph( ss.ParagraphStyles.Normal )
        p.append( relatorio["f8_1"].replace("\n","").decode('utf-8').encode('rtfunicode'))
        #Note: it gives problems in the rtf. But why \n? My 1st guess was deleting all \r. I think it is rtfunicode, encodes \n to something else.
        section.append( p )

        p = rtf.Paragraph( ss.ParagraphStyles.Normal )
        p.append( "" )
        section.append( p )

        p = rtf.Paragraph( ss.ParagraphStyles.Normal )
        p.append( "8.2. Publicações (indicar o título e se está em preparação, em fase de avaliação ou no prelo):".decode('utf-8').encode('rtfunicode'))
        section.append( p )

        p = rtf.Paragraph( ss.ParagraphStyles.Normal )
        p.append( relatorio["f8_2"].replace("\n","").decode('utf-8').encode('rtfunicode'))
        #Note: it gives problems in the rtf. But why \n? My 1st guess was deleting all \r. I think it is rtfunicode, encodes \n to something else.
        section.append( p )

        p = rtf.Paragraph( ss.ParagraphStyles.Normal )
        p.append( "" )
        section.append( p )

        p = rtf.Paragraph( ss.ParagraphStyles.Normal )
        p.append( "8.3. Comunicações em colóquios (indicar URL do evento, título (provisório) da comunicação):".decode('utf-8').encode('rtfunicode'))
        section.append( p )

        p = rtf.Paragraph( ss.ParagraphStyles.Normal )
        p.append( relatorio["f8_3"].replace("\n","").decode('utf-8').encode('rtfunicode'))
        #Note: it gives problems in the rtf. But why \n? My 1st guess was deleting all \r. I think it is rtfunicode, encodes \n to something else.
        section.append( p )

        p = rtf.Paragraph( ss.ParagraphStyles.Normal )
        p.append( "" )
        section.append( p )

        p = rtf.Paragraph( ss.ParagraphStyles.Normal )
        p.append( "8.4. Organização de colóquios e seminários (indicar tipo de evento, local, equipa organizadora, financiamento):".decode('utf-8').encode('rtfunicode'))
        section.append( p )

        p = rtf.Paragraph( ss.ParagraphStyles.Normal )
        p.append( relatorio["f8-4"].replace("\n","").decode('utf-8').encode('rtfunicode'))
        #Note: it gives problems in the rtf. But why \n? My 1st guess was deleting all \r. I think it is rtfunicode, encodes \n to something else.
        section.append( p )
        '''

        response.headers['Content-Type']='text/rtf'
        return rtf.dumps(doc)
    else:
        #raise HTTP(404,'Erro: login inválio, tente realizar novo login.')
        redirect(URL('default', 'edit.html'))

def __html(form): # __ makes function private to web2py
    form = form.first() #first() makes it easier to itinerate

    p = ""


    html = BODY(
        H1("Relatório "+YEAR),
        BR(),
        H1("Dados Pessoais:"),
        )

    for t in fields_personal:
        html.append(P(B(t[0]+" "), form[t[1]]),)

    for t in fields_data:
        if t[1] == '':
            html.append(BR(),)
            html.append(H1(t[0]),)
        else:
            html.append(BR(),)
            html.append(H2(t[0]),)
            html.append(PRE(form[t[1]]),)

    '''
        BR(),
        H1("Dados Pessoais:"),
        P(B("Nome: "), form["nome"]),
        P(B("E-mail: "), form["email"]),
        P(B("Categoria Universitária: "), form["categoria"]),
        P(B("Grau Académico: "), form["grau"]),
        P(B("Linha Temática: "), form["linha"]),

        BR(),


        H1("1. Projeto de investigação em curso"),
        H2("1. 1. Projeto de Mestrado, Doutoramento, Pós-Doutoramento (indicar título, data de início e supervisor):"),
        PRE(form["f1_1"]),

        BR(),

        H2("1.2. Projeto(s) de investigação financiado(s) pela FCT ou outra entidade (indicar título, data de início e investigador responsável):"),
        PRE(form["f1_2"]),

        BR(),

        H1("2. Internacionalização"),
        H2("2.1. Participação em Redes Internacionais e Europeias (indicar título, instituiçãoresponsável, financiamento e data de início e fim, se aplicável, e URL):"),
        PRE(form["f2_1"]),

        BR(),

        H1("3. Publicações de caráter científico"),
        H2("Nota importante: As referências das publicações devem ser indicadas em formato APA, com indicação do URL (referente ao depósito em repositório / link da revista / DOI / indexação / e-Book)."),
        H2("3.1. Livros (indicar se é monografia, volume editado...):"),
        PRE(form["f3_1"]),

        BR(),

        H2("3.2. Capítulos de livros (indicar se for peer review)"),
        H2("3.2.1 Capítulos de livros nacionais:"),
        PRE(form["f3_2_1"]),

        BR(),

        H2("3.2.2 Capítulos de livros internacionais:"),
        PRE(form["f3_2_2"]),

        BR(),

        H2("3.3. Artigos em livros de atas:"),
        PRE(form["f3_3"]),

        BR(),

        H2("3.4. Publicações em Revistas (indicar se for peer review)"),
        H2("3.4.1. Artigos em revistas nacionais: "),
        PRE(form["f3_4_1"]),

        BR(),

        H2("3.4.2 Artigos em Revistas internacionais:"),
        PRE(form["f3_4_2"]),

        BR(),

        H2("3.5. Outras publicações (não indicar publicações no prelo):"),
        PRE(form["f3_5"]),

        BR(),

        H1("4. Comunicações"),
        H2("4.1. Encontros científicos nacionais (indicar se por convite ou submissão de resumo):"),
        PRE(form["f4_1"]),

        BR(),

        H2("4.2. Encontros científicos internacionais (indicar se por convite ou submissão de resumo):"),
        PRE(form["f4_2"]),

        BR(),

        H1("5. Organização de Colóquios ou Seminários"),
        H2("Indicar título, data, local, o URL do evento."),
        PRE(form["f5"]),

        BR(),

        H1("6. Orientação de teses concluídas em 2018"),
        H2("6.1. Doutoramentos (título, data de defesa e nome do candidato):"),
        PRE(form["f6_1"]),

        BR(),

        H2("6.2. Mestrados (título, data de defesa e nome do candidato):"),
        PRE(form["f6_2"]),

        BR(),

        H1("7. Participação em equipas editoriais e comissões científicas de revistas (indicar cargo, revista e URL)"),
        PRE(form["f7"]),

        BR(),

        H1("8. Atividades previstas para 2019"),
        H2("8.1. Atividades de investigação no âmbito de projetos (nomeadamente projetos a submeter a financiamento):"),
        PRE(form["f8_1"]),

        BR(),

        H2("8.2. Publicações (indicar o título e se está em preparação, em fase de avaliação ou no prelo):"),
        PRE(form["f8_2"]),

        BR(),

        H2("8.3. Comunicações em colóquios (indicar URL do evento, título (provisório) da comunicação):"),
        PRE(form["f8_3"]),

        BR(),

        H2("8.4. Organização de colóquios e seminários (indicar tipo de evento, local, equipa organizadora, financiamento):"),
        PRE(form["f8-4"]),

    )
    '''
    return html

def export_html():
    try:
        relatorio = db(db.relatorio.private_link == session.code).select()
        password = relatorio.first()["password"]
    except:
        password = None

    if session.password and password == session.password:
        html = __html(relatorio)
        if request.function == "export_html":
            return dict(html=html)
        else:
            return html
    else:
        redirect(URL('default', 'edit.html'))

def export_pdf():
    try:
        relatorio = db(db.relatorio.private_link == session.code).select()
        password = relatorio.first()["password"]
    except:
        password = None

    if session.password and password == session.password:
        relatorio = relatorio.first() #first() makes easier to ininerate

        from fpdf import FPDF
        #não usei a versão do pyFPDF default do web2py no gluon por não ser a mais recente.
        #Desta forma também dá mais liberdade, em vez de usar generic views

        pdf = FPDF()
        pdf.set_margins(left=20,top=20,right=-1)
        pdf.add_page()

        #pdf.add_font('DejaVu', '', 'DejaVuSans.ttf', uni=True)
        #pdf.add_font('DejaVu', 'B', 'DejaVuSans-Bold.ttf', uni=True) #Dozens of idioms. But no chinese :(
        #pdf.add_font('fireflysung', '', 'fireflysung.ttf', uni=True) #chinese/japanese

        pdf.add_font('ArialUnicode', '', 'ArialUnicodeMS.ttf', uni=True) #A lot of idioms, european and chinese. But licensed... No Bold version

        pdf.set_font('ArialUnicode', '', 14)

        pdf.set_font('Arial','B',18)
        pdf.set_text_color(178, 34, 34)
        pdf.multi_cell(0,7, u'Relatório Científico '+YEAR, align = 'L')

        pdf.ln(h='14')

        pdf.set_text_color(0, 0, 0)
        pdf.set_font('Arial','B',14)

        pdf.multi_cell(0,7, u'Dados Pessoais:', align = 'L')

        for t in fields_personal:
            pdf.ln(h='14')
            pdf.set_font('Arial','B',12)
            pdf.write(7, t[0].decode("utf-8") + " ")
            pdf.set_font('ArialUnicode','',12)
            pdf.write(7, relatorio[t[1]])

        pdf.ln(h='14')
        pdf.ln(h='14')

        for t in fields_data:
            if t[1] == '':
                pdf.ln(h='14')
                pdf.set_font('Arial','B',14)
                pdf.multi_cell(0,7, t[0].decode("utf-8"), align = 'L')
            else:
                pdf.ln(h='14')
                pdf.set_font('Arial','B',14)
                pdf.multi_cell(0,7, t[0].decode("utf-8"), align = 'L')
                pdf.set_font('ArialUnicode','',12)
                pdf.multi_cell(0,7,relatorio[t[1]], align = 'L')

        """
        pdf.ln(h='14')

        pdf.set_font('Arial','B',12)
        pdf.write(7, u'E-mail: ')
        pdf.set_font('ArialUnicode','',12)
        pdf.write(7, relatorio['email'])

        pdf.ln(h='14')

        pdf.set_font('Arial','B',12)
        pdf.write(7, u'Categoria Académica: ')
        pdf.set_font('ArialUnicode','',12)
        pdf.write(7, relatorio['categoria'])

        pdf.ln(h='14')

        pdf.set_font('Arial','B',12)
        pdf.write(7, u'Grau: ')
        pdf.set_font('ArialUnicode','',12)
        pdf.write(7, relatorio['grau'])

        pdf.ln(h='14')

        pdf.set_font('Arial','B',12)
        pdf.write(7, u'Linha Temática: ')
        pdf.set_font('ArialUnicode','',12)
        pdf.write(7, relatorio['linha'])

        pdf.ln(h='14')
        pdf.ln(h='14')

        pdf.set_font('Arial','B',14)
        pdf.multi_cell(0,7, u'1. Projeto de investigação em curso', align = 'L')
        pdf.set_font('Arial','B',12)
        pdf.multi_cell(0,7, u'1. 1. Projeto de Mestrado, Doutoramento, Pós-Doutoramento (indicar título, data de início e supervisor):', align = 'L')
        pdf.set_font('ArialUnicode','',12)
        pdf.multi_cell(0,7,relatorio['f1_1'], align = 'L')

        pdf.ln(h='14')

        pdf.set_font('Arial','B',12)
        pdf.multi_cell(0,7, u'1.2. Projeto(s) de investigação financiado(s) pela FCT ou outra entidade (indicar título, data de início e investigador responsável):', align = 'L')
        pdf.set_font('ArialUnicode','',12)
        pdf.multi_cell(0,7,relatorio['f1_2'], align = 'L')

        pdf.ln(h='14')

        pdf.set_font('Arial','B',14)
        pdf.multi_cell(0,7, u'2. Internacionalização', align = 'L')
        pdf.set_font('Arial','B',12)
        pdf.multi_cell(0,7, u'2.1. Participação em Redes Internacionais e Europeias (indicar título, instituição responsável, financiamento e data de início e fim, se aplicável, e URL):', align = 'L')
        pdf.set_font('ArialUnicode','',12)
        pdf.multi_cell(0,7,relatorio['f2_1'], align = 'L')

        pdf.ln(h='14')

        pdf.set_font('Arial','B',14)
        pdf.multi_cell(0,7, u'3. Publicações de caráter científico', align = 'L')
        pdf.set_font('Arial','B',12)
        pdf.multi_cell(0,7, u'Nota importante: As referências das publicações devem ser indicadas em formato APA, com indicação do URL (referente ao depósito em repositório / link da revista / DOI / indexação / e-Book).', align = 'L')
        pdf.set_font('Arial','B',12)
        pdf.multi_cell(0,7, u'3.1. Livros (indicar se é monografia, volume editado...):', align = 'L')
        pdf.set_font('ArialUnicode','',12)
        pdf.multi_cell(0,7,relatorio['f3_1'], align = 'L')

        pdf.ln(h='14')

        pdf.set_font('Arial','B',12)
        pdf.multi_cell(0,7, u'3.2. Capítulos de livros (indicar se for peer review)', align = 'L')
        pdf.multi_cell(0,7, u'3.2.1 Capítulos de livros nacionais:', align = 'L')
        pdf.set_font('ArialUnicode','',12)
        pdf.multi_cell(0,7,relatorio['f3_2_1'], align = 'L')

        pdf.ln(h='14')

        pdf.set_font('Arial','B',12)
        pdf.multi_cell(0,7, u'3.2.2 Capítulos de livros internacionais:', align = 'L')
        pdf.set_font('ArialUnicode','',12)
        pdf.multi_cell(0,7,relatorio['f3_2_2'], align = 'L')

        pdf.ln(h='14')

        pdf.set_font('Arial','B',12)
        pdf.multi_cell(0,7, u'3.3. Artigos em livros de atas:', align = 'L')
        pdf.set_font('ArialUnicode','',12)
        pdf.multi_cell(0,7,relatorio['f3_3'], align = 'L')

        pdf.ln(h='14')

        pdf.set_font('Arial','B',12)
        pdf.multi_cell(0,7, u'3.4. Publicações em Revistas (indicar se for peer review)', align = 'L')
        pdf.multi_cell(0,7, u'3.4.1. Artigos em revistas nacionais:', align = 'L')
        pdf.set_font('ArialUnicode','',12)
        pdf.multi_cell(0,7,relatorio['f3_4_1'], align = 'L')

        pdf.ln(h='14')

        pdf.set_font('Arial','B',12)
        pdf.multi_cell(0,7, u'3.4. 2 Artigos em revistas internacionais:', align = 'L')
        pdf.set_font('ArialUnicode','',12)
        pdf.multi_cell(0,7,relatorio['f3_4_2'], align = 'L')

        pdf.ln(h='14')

        pdf.set_font('Arial','B',12)
        pdf.multi_cell(0,7, u'3.5.  Outras publicações (não indicar publicações no prelo):', align = 'L')
        pdf.set_font('ArialUnicode','',12)
        pdf.multi_cell(0,7,relatorio['f3_5'], align = 'L')

        pdf.ln(h='14')

        pdf.set_font('Arial','B',14)
        pdf.multi_cell(0,7, u'4. Comunicações', align = 'L')
        pdf.set_font('Arial','B',12)
        pdf.multi_cell(0,7, u'4.1. Encontros científicos nacionais (indicar se por convite ou submissão de resumo):', align = 'L')
        pdf.set_font('ArialUnicode','',12)
        pdf.multi_cell(0,7,relatorio['f4_1'], align = 'L')

        pdf.ln(h='14')

        pdf.set_font('Arial','B',12)
        pdf.multi_cell(0,7, u'4.2. Encontros científicos internacionais (indicar se por convite ou submissão de resumo):', align = 'L')
        pdf.set_font('ArialUnicode','',12)
        pdf.multi_cell(0,7,relatorio['f4_2'], align = 'L')

        pdf.ln(h='14')

        pdf.set_font('Arial','B',14)
        pdf.multi_cell(0,7, u'5. Organização de Colóquios ou Seminários', align = 'L')
        pdf.multi_cell(0,7, u'Indicar título, data, local, o URL do evento.', align = 'L')
        pdf.set_font('ArialUnicode','',12)
        pdf.multi_cell(0,7,relatorio['f5'], align = 'L')

        pdf.ln(h='14')

        pdf.set_font('Arial','B',14)
        pdf.multi_cell(0,7, u'6. Orientação de teses concluídas em 2018', align = 'L')
        pdf.set_font('Arial','B',12)
        pdf.multi_cell(0,7, u'6.1. Doutoramentos (título, data de defesa e nome do candidato):', align = 'L')
        pdf.set_font('ArialUnicode','',12)
        pdf.multi_cell(0,7,relatorio['f6_1'], align = 'L')

        pdf.ln(h='14')

        pdf.set_font('Arial','B',12)
        pdf.multi_cell(0,7, u'6.2. Mestrados (título, data de defesa e nome do candidato):', align = 'L')
        pdf.set_font('ArialUnicode','',12)
        pdf.multi_cell(0,7,relatorio['f6_2'], align = 'L')

        pdf.ln(h='14')

        pdf.set_font('Arial','B',14)
        pdf.multi_cell(0,7, u'7. Participação em equipas editoriais e comissões científicas de revistas (indicar cargo, revista e URL)', align = 'L')
        pdf.set_font('ArialUnicode','',12)
        pdf.multi_cell(0,7,relatorio['f7'], align = 'L')

        pdf.ln(h='14')

        pdf.set_font('Arial','B',14)
        pdf.multi_cell(0,7, u'8. Atividades previstas para 2019', align = 'L')
        pdf.set_font('Arial','B',12)
        pdf.multi_cell(0,7, u'8.1. Atividades de investigação no âmbito de projetos (nomeadamente projetos a submeter a financiamento):', align = 'L')
        pdf.set_font('ArialUnicode','',12)
        pdf.multi_cell(0,7,relatorio['f8_1'], align = 'L')

        pdf.ln(h='14')

        pdf.set_font('Arial','B',14)
        pdf.multi_cell(0,7, u'8.2. Publicações (indicar o título e se está em preparação, em fase de avaliação ou no prelo):', align = 'L')
        pdf.set_font('ArialUnicode','',12)
        pdf.multi_cell(0,7,relatorio['f8_2'], align = 'L')

        pdf.ln(h='14')

        pdf.set_font('Arial','B',14)
        pdf.multi_cell(0,7, u'8.3. Comunicações em colóquios (indicar URL do evento, título (provisório) da comunicação):', align = 'L')
        pdf.set_font('ArialUnicode','',12)
        pdf.multi_cell(0,7,relatorio['f8_3'], align = 'L')

        pdf.ln(h='14')

        pdf.set_font('Arial','B',14)
        pdf.multi_cell(0,7, u'8.4. Organização de colóquios e seminários (indicar tipo de evento, local, equipa organizadora, financiamento):', align = 'L')
        pdf.set_font('ArialUnicode','',12)
        pdf.multi_cell(0,7,relatorio['f8-4'], align = 'L')
        """

        return pdf.output(dest='S')

    else:
        redirect(URL('default', 'edit.html'))

def export_csv():
    try:
        relatorio = db(db.relatorio.private_link == session.code).select()
        password = relatorio.first()["password"]
    except:
        password = None

    if session.password and password == session.password:
        return dict(relatorio=relatorio)
    else:
        redirect(URL('default', 'edit.html'))

def ver():
    """if request.vars["linha"] == "linguagem":
        linha = "Ciências da Linguagem"
    elif request.vars["linha"] == "filosofia_cultura":
        linha = "Filosofia e Cultura"
    elif request.vars["linha"] == "literatura":
        linha = "Ciências da Literatura"
    else:
        raise HTTP(404,"404 PAGE NOT FOUND/DON'T EXIST")
    """
    #emails = db(db.relatorio.id > 0).select(db.relatorio.email)
    db.relatorio.data.readable=True

    table = SQLFORM.grid(db.relatorio.id>0, orderby=~db.relatorio.id, buttons_placement="left", sortable=True, csv=True, searchable=True, create=False, paginate=300, deletable=False, editable=False, ignore_rw=False)

    return locals()


def ver_relatorio_rtf():
    if request.vars["linha"] == "linguagem":
        linha = "Ciências da Linguagem"
    elif request.vars["linha"] == "filosofia_cultura":
        linha = "Filosofia e Cultura"
    elif request.vars["linha"] == "literatura":
        linha = "Ciências da Literatura"
    else:
        raise HTTP(500,'message')

    form_doutor = relatorio = db( (db.relatorio.linha == linha) & (db.relatorio.grau == "Doutor") ).select(orderby=db.relatorio.nome)
    form_nao_doutor = relatorio = db( (db.relatorio.linha == linha) & (db.relatorio.grau != "Doutor") ).select(orderby=db.relatorio.nome)

    import gluon.contrib.pyrtf as rtf
    import rtfunicode
    #had problems with encodings, chinese and accented letters to rtf, found this rtfunicode script in:
    #http://www.zopatista.com/python/2012/06/06/rtf-and-unicode/
    #github in: https://github.com/mjpieters/rtfunicode

    doc=rtf.Document()
    ss = doc.StyleSheet
    section=rtf.Section()
    doc.Sections.append(section)

    h1 = rtf.Paragraph( ss.ParagraphStyles.Heading1 )
    h1.append( "Relatório Científico 2018".decode("utf-8").encode('rtfunicode') )
    section.append(h1)

    h1 = rtf.Paragraph( ss.ParagraphStyles.Heading1 )
    h1.append( "Linha Temática".decode("utf-8").encode('rtfunicode') )
    section.append(h1)

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "" )
    section.append( p )

    h2 = rtf.Paragraph( ss.ParagraphStyles.Heading2 )
    h2.append( "1. Projecto de investigação em curso:".decode("utf-8").encode('rtfunicode') )
    section.append(h2)

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "1. 1. Projeto de Mestrado, Doutoramento, Pós-Doutoramento (indicar título, data de início e supervisor):".decode('utf-8').encode('rtfunicode'))
    section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "" )
    section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "DOUTORADOS".decode('utf-8').encode('rtfunicode'))
    section.append( p )

    for doutor in form_doutor:
        if doutor["f1_1"]:
            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( doutor["nome"].replace("\n","").decode('utf-8').encode('rtfunicode'))
            section.append( p )

            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( doutor["f1_1"].replace("\n","").decode('utf-8').encode('rtfunicode'))
            section.append( p )

            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( "" )
            section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "" )
    section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "NÃO-DOUTORADOS".decode('utf-8').encode('rtfunicode'))
    section.append( p )

    for nao_dout in form_nao_doutor:
        if nao_dout["f1_1"]:
            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( nao_dout["nome"].replace("\n","").decode('utf-8').encode('rtfunicode'))
            section.append( p )

            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( nao_dout["f1_1"].replace("\n","").decode('utf-8').encode('rtfunicode'))
            section.append( p )

            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( "" )
            section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "" )
    section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "1.2. Projeto(s) de investigação financiado(s) pela FCT ou outra entidade (indicar título, data de início e investigador responsável):".decode('utf-8').encode('rtfunicode'))
    section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "" )
    section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "DOUTORADOS".decode('utf-8').encode('rtfunicode'))
    section.append( p )

    for doutor in form_doutor:
        if doutor["f1_2"]:
            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( doutor["nome"].replace("\n","").decode('utf-8').encode('rtfunicode'))
            section.append( p )

            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( doutor["f1_2"].replace("\n","").decode('utf-8').encode('rtfunicode'))
            section.append( p )

            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( "" )
            section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "" )
    section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "NÃO-DOUTORADOS".decode('utf-8').encode('rtfunicode'))
    section.append( p )

    for nao_dout in form_nao_doutor:
        if nao_dout["f1_2"]:
            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( nao_dout["nome"].replace("\n","").decode('utf-8').encode('rtfunicode'))
            section.append( p )

            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( nao_dout["f1_2"].replace("\n","").decode('utf-8').encode('rtfunicode'))
            section.append( p )

            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( "" )
            section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "" )
    section.append( p )

    h2 = rtf.Paragraph( ss.ParagraphStyles.Heading2 )
    h2.append( "2. Internacionalização".decode("utf-8").encode('rtfunicode') )
    section.append(h2)

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "2.1. Participação em Redes Internacionais e Europeias (indicar título, instituição responsável, financiamento e data de início e fim, se aplicável, e URL):".decode('utf-8').encode('rtfunicode'))
    section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "" )
    section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "DOUTORADOS".decode('utf-8').encode('rtfunicode'))
    section.append( p )

    for doutor in form_doutor:
        if doutor["f2_1"]:
            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( doutor["nome"].replace("\n","").decode('utf-8').encode('rtfunicode'))
            section.append( p )

            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( doutor["f2_1"].replace("\n","").decode('utf-8').encode('rtfunicode'))
            section.append( p )

            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( "" )
            section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "" )
    section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "NÃO-DOUTORADOS".decode('utf-8').encode('rtfunicode'))
    section.append( p )

    for nao_dout in form_nao_doutor:
        if nao_dout["f2_1"]:
            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( nao_dout["nome"].replace("\n","").decode('utf-8').encode('rtfunicode'))
            section.append( p )

            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( nao_dout["f2_1"].replace("\n","").decode('utf-8').encode('rtfunicode'))
            section.append( p )

            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( "" )
            section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "" )
    section.append( p )

    h2 = rtf.Paragraph( ss.ParagraphStyles.Heading2 )
    h2.append( "3. Publicações de caráter científico".decode("utf-8").encode('rtfunicode') )
    section.append(h2)

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "Nota importante: As referências das publicações devem ser indicadas em formato APA, com indicação do URL (referente ao depósito em repositório / link da revista / DOI / indexação / e-Book).".decode('utf-8').encode('rtfunicode'))
    section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "3.1. Livros (indicar se é monografia, volume editado...):".decode('utf-8').encode('rtfunicode'))
    section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "" )
    section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "DOUTORADOS".decode('utf-8').encode('rtfunicode'))
    section.append( p )

    for doutor in form_doutor:
        if doutor["f3_1"]:
            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( doutor["nome"].replace("\n","").decode('utf-8').encode('rtfunicode'))
            section.append( p )

            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( doutor["f3_1"].replace("\n","").decode('utf-8').encode('rtfunicode'))
            section.append( p )

            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( "" )
            section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "" )
    section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "NÃO-DOUTORADOS".decode('utf-8').encode('rtfunicode'))
    section.append( p )

    for nao_dout in form_nao_doutor:
        if nao_dout["f3_1"]:
            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( nao_dout["nome"].replace("\n","").decode('utf-8').encode('rtfunicode'))
            section.append( p )

            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( nao_dout["f3_1"].replace("\n","").decode('utf-8').encode('rtfunicode'))
            section.append( p )

            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( "" )
            section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "" )
    section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "3.2. Capítulos de livros (indicar se for peer review)".decode('utf-8').encode('rtfunicode'))
    section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "3.2.1 Capítulos de livros nacionais:".decode('utf-8').encode('rtfunicode'))
    section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "" )
    section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "DOUTORADOS".decode('utf-8').encode('rtfunicode'))
    section.append( p )

    for doutor in form_doutor:
        if doutor["f3_2_1"]:
            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( doutor["nome"].replace("\n","").decode('utf-8').encode('rtfunicode'))
            section.append( p )

            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( doutor["f3_2_1"].replace("\n","").decode('utf-8').encode('rtfunicode'))
            section.append( p )

            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( "" )
            section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "" )
    section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "NÃO-DOUTORADOS".decode('utf-8').encode('rtfunicode'))
    section.append( p )

    for nao_dout in form_nao_doutor:
        if nao_dout["f3_2_1"]:
            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( nao_dout["nome"].replace("\n","").decode('utf-8').encode('rtfunicode'))
            section.append( p )

            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( nao_dout["f3_2_1"].replace("\n","").decode('utf-8').encode('rtfunicode'))
            section.append( p )

            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( "" )
            section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "" )
    section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "3.2.2 Capítulos de livros internacionais:".decode('utf-8').encode('rtfunicode'))
    section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "" )
    section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "DOUTORADOS".decode('utf-8').encode('rtfunicode'))
    section.append( p )

    for doutor in form_doutor:
        if doutor["f3_2_2"]:
            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( doutor["nome"].replace("\n","").decode('utf-8').encode('rtfunicode'))
            section.append( p )

            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( doutor["f3_2_2"].replace("\n","").decode('utf-8').encode('rtfunicode'))
            section.append( p )

            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( "" )
            section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "" )
    section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "NÃO-DOUTORADOS".decode('utf-8').encode('rtfunicode'))
    section.append( p )

    for nao_dout in form_nao_doutor:
        if nao_dout["f3_2_2"]:
            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( nao_dout["nome"].replace("\n","").decode('utf-8').encode('rtfunicode'))
            section.append( p )

            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( nao_dout["f3_2_2"].replace("\n","").decode('utf-8').encode('rtfunicode'))
            section.append( p )

            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( "" )
            section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "" )
    section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "3.3. Artigos em livros de atas:".decode('utf-8').encode('rtfunicode'))
    section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "" )
    section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "DOUTORADOS".decode('utf-8').encode('rtfunicode'))
    section.append( p )

    for doutor in form_doutor:
        if doutor["f3_3"]:
            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( doutor["nome"].replace("\n","").decode('utf-8').encode('rtfunicode'))
            section.append( p )

            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( doutor["f3_3"].replace("\n","").decode('utf-8').encode('rtfunicode'))
            section.append( p )

            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( "" )
            section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "" )
    section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "NÃO-DOUTORADOS".decode('utf-8').encode('rtfunicode'))
    section.append( p )

    for nao_dout in form_nao_doutor:
        if nao_dout["f3_3"]:
            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( nao_dout["nome"].replace("\n","").decode('utf-8').encode('rtfunicode'))
            section.append( p )

            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( nao_dout["f3_3"].replace("\n","").decode('utf-8').encode('rtfunicode'))
            section.append( p )

            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( "" )
            section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "" )
    section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "3.4. Publicações em Revistas (indicar se for peer review)".decode('utf-8').encode('rtfunicode'))
    section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "3.4.1. Artigos em revistas nacionais:".decode('utf-8').encode('rtfunicode'))
    section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "" )
    section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "DOUTORADOS".decode('utf-8').encode('rtfunicode'))
    section.append( p )

    for doutor in form_doutor:
        if doutor["f3_4_1"]:
            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( doutor["nome"].replace("\n","").decode('utf-8').encode('rtfunicode'))
            section.append( p )

            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( doutor["f3_4_1"].replace("\n","").decode('utf-8').encode('rtfunicode'))
            section.append( p )

            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( "" )
            section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "" )
    section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "NÃO-DOUTORADOS".decode('utf-8').encode('rtfunicode'))
    section.append( p )

    for nao_dout in form_nao_doutor:
        if nao_dout["f3_4_1"]:
            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( nao_dout["nome"].replace("\n","").decode('utf-8').encode('rtfunicode'))
            section.append( p )

            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( nao_dout["f3_4_1"].replace("\n","").decode('utf-8').encode('rtfunicode'))
            section.append( p )

            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( "" )
            section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "" )
    section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "3.4.2 Artigos em revistas internacionais:".decode('utf-8').encode('rtfunicode'))
    section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "" )
    section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "DOUTORADOS".decode('utf-8').encode('rtfunicode'))
    section.append( p )

    for doutor in form_doutor:
        if doutor["f3_4_2"]:
            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( doutor["nome"].replace("\n","").decode('utf-8').encode('rtfunicode'))
            section.append( p )

            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( doutor["f3_4_2"].replace("\n","").decode('utf-8').encode('rtfunicode'))
            section.append( p )

            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( "" )
            section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "" )
    section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "NÃO-DOUTORADOS".decode('utf-8').encode('rtfunicode'))
    section.append( p )

    for nao_dout in form_nao_doutor:
        if nao_dout["f3_4_2"]:
            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( nao_dout["nome"].replace("\n","").decode('utf-8').encode('rtfunicode'))
            section.append( p )

            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( nao_dout["f3_4_2"].replace("\n","").decode('utf-8').encode('rtfunicode'))
            section.append( p )

            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( "" )
            section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "" )
    section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "3.5 Outras publicações (não indicar publicações no prelo):".decode('utf-8').encode('rtfunicode'))
    section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "" )
    section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "DOUTORADOS".decode('utf-8').encode('rtfunicode'))
    section.append( p )

    for doutor in form_doutor:
        if doutor["f3_5"]:
            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( doutor["nome"].replace("\n","").decode('utf-8').encode('rtfunicode'))
            section.append( p )

            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( doutor["f3_5"].replace("\n","").decode('utf-8').encode('rtfunicode'))
            section.append( p )

            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( "" )
            section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "" )
    section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "NÃO-DOUTORADOS".decode('utf-8').encode('rtfunicode'))
    section.append( p )

    for nao_dout in form_nao_doutor:
        if nao_dout["f3_5"]:
            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( nao_dout["nome"].replace("\n","").decode('utf-8').encode('rtfunicode'))
            section.append( p )

            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( nao_dout["f3_5"].replace("\n","").decode('utf-8').encode('rtfunicode'))
            section.append( p )

            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( "" )
            section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "" )
    section.append( p )

    h2 = rtf.Paragraph( ss.ParagraphStyles.Heading2 )
    h2.append( "4. Comunicações".decode("utf-8").encode('rtfunicode') )
    section.append(h2)

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "4.1. Encontros científicos nacionais (indicar se por convite ou submissão de resumo):".decode('utf-8').encode('rtfunicode'))
    section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "" )
    section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "DOUTORADOS".decode('utf-8').encode('rtfunicode'))
    section.append( p )

    for doutor in form_doutor:
        if doutor["f4_1"]:
            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( doutor["nome"].replace("\n","").decode('utf-8').encode('rtfunicode'))
            section.append( p )

            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( doutor["f4_1"].replace("\n","").decode('utf-8').encode('rtfunicode'))
            section.append( p )

            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( "" )
            section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "" )
    section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "NÃO-DOUTORADOS".decode('utf-8').encode('rtfunicode'))
    section.append( p )

    for nao_dout in form_nao_doutor:
        if nao_dout["f4_1"]:
            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( nao_dout["nome"].replace("\n","").decode('utf-8').encode('rtfunicode'))
            section.append( p )

            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( nao_dout["f4_1"].replace("\n","").decode('utf-8').encode('rtfunicode'))
            section.append( p )

            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( "" )
            section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "" )
    section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "4.2. Encontros científicos internacionais (indicar se por convite ou submissão de resumo):".decode('utf-8').encode('rtfunicode'))
    section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "" )
    section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "DOUTORADOS".decode('utf-8').encode('rtfunicode'))
    section.append( p )

    for doutor in form_doutor:
        if doutor["f4_2"]:
            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( doutor["nome"].replace("\n","").decode('utf-8').encode('rtfunicode'))
            section.append( p )

            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( doutor["f4_2"].replace("\n","").decode('utf-8').encode('rtfunicode'))
            section.append( p )

            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( "" )
            section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "" )
    section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "NÃO-DOUTORADOS".decode('utf-8').encode('rtfunicode'))
    section.append( p )

    for nao_dout in form_nao_doutor:
        if nao_dout["f4_2"]:
            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( nao_dout["nome"].replace("\n","").decode('utf-8').encode('rtfunicode'))
            section.append( p )

            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( nao_dout["f4_2"].replace("\n","").decode('utf-8').encode('rtfunicode'))
            section.append( p )

            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( "" )
            section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "" )
    section.append( p )

    h2 = rtf.Paragraph( ss.ParagraphStyles.Heading2 )
    h2.append( "5. Organização de Colóquios ou Seminários".decode("utf-8").encode('rtfunicode') )
    section.append(h2)

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "Indicar título, data, local, o URL do evento.".decode('utf-8').encode('rtfunicode'))
    section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "" )
    section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "DOUTORADOS".decode('utf-8').encode('rtfunicode'))
    section.append( p )

    for doutor in form_doutor:
        if doutor["f5"]:
            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( doutor["nome"].replace("\n","").decode('utf-8').encode('rtfunicode'))
            section.append( p )

            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( doutor["f5"].replace("\n","").decode('utf-8').encode('rtfunicode'))
            section.append( p )

            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( "" )
            section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "" )
    section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "NÃO-DOUTORADOS".decode('utf-8').encode('rtfunicode'))
    section.append( p )

    for nao_dout in form_nao_doutor:
        if nao_dout["f5"]:
            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( nao_dout["nome"].replace("\n","").decode('utf-8').encode('rtfunicode'))
            section.append( p )

            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( nao_dout["f5"].replace("\n","").decode('utf-8').encode('rtfunicode'))
            section.append( p )

            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( "" )
            section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "" )
    section.append( p )

    h2 = rtf.Paragraph( ss.ParagraphStyles.Heading2 )
    h2.append( "6. Orientação de teses concluídas em 2018".decode("utf-8").encode('rtfunicode') )
    section.append(h2)

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "6.1. Doutoramentos (título, data de defesa e nome do candidato):".decode('utf-8').encode('rtfunicode'))
    section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "" )
    section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "DOUTORADOS".decode('utf-8').encode('rtfunicode'))
    section.append( p )

    for doutor in form_doutor:
        if doutor["f6_1"]:
            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( doutor["nome"].replace("\n","").decode('utf-8').encode('rtfunicode'))
            section.append( p )

            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( doutor["f6_1"].replace("\n","").decode('utf-8').encode('rtfunicode'))
            section.append( p )

            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( "" )
            section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "" )
    section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "NÃO-DOUTORADOS".decode('utf-8').encode('rtfunicode'))
    section.append( p )

    for nao_dout in form_nao_doutor:
        if nao_dout["f6_1"]:
            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( nao_dout["nome"].replace("\n","").decode('utf-8').encode('rtfunicode'))
            section.append( p )

            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( nao_dout["f6_1"].replace("\n","").decode('utf-8').encode('rtfunicode'))
            section.append( p )

            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( "" )
            section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "" )
    section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "6.2. Mestrados (título, data de defesa e nome do candidato): ".decode('utf-8').encode('rtfunicode'))
    section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "" )
    section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "DOUTORADOS".decode('utf-8').encode('rtfunicode'))
    section.append( p )

    for doutor in form_doutor:
        if doutor["f6_2"]:
            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( doutor["nome"].replace("\n","").decode('utf-8').encode('rtfunicode'))
            section.append( p )

            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( doutor["f6_2"].replace("\n","").decode('utf-8').encode('rtfunicode'))
            section.append( p )

            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( "" )
            section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "" )
    section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "NÃO-DOUTORADOS".decode('utf-8').encode('rtfunicode'))
    section.append( p )

    for nao_dout in form_nao_doutor:
        if nao_dout["f6_2"]:
            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( nao_dout["nome"].replace("\n","").decode('utf-8').encode('rtfunicode'))
            section.append( p )

            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( nao_dout["f6_2"].replace("\n","").decode('utf-8').encode('rtfunicode'))
            section.append( p )

            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( "" )
            section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "" )
    section.append( p )

    h2 = rtf.Paragraph( ss.ParagraphStyles.Heading2 )
    h2.append( "7. Participação em equipas editoriais e comissões científicas de revistas (indicar cargo, revista e URL)".decode("utf-8").encode('rtfunicode') )
    section.append(h2)

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "" )
    section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "DOUTORADOS".decode('utf-8').encode('rtfunicode'))
    section.append( p )

    for doutor in form_doutor:
        if doutor["f7"]:
            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( doutor["nome"].replace("\n","").decode('utf-8').encode('rtfunicode'))
            section.append( p )

            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( doutor["f7"].replace("\n","").decode('utf-8').encode('rtfunicode'))
            section.append( p )

            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( "" )
            section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "" )
    section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "NÃO-DOUTORADOS".decode('utf-8').encode('rtfunicode'))
    section.append( p )

    for nao_dout in form_nao_doutor:
        if nao_dout["f7"]:
            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( nao_dout["nome"].replace("\n","").decode('utf-8').encode('rtfunicode'))
            section.append( p )

            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( nao_dout["f7"].replace("\n","").decode('utf-8').encode('rtfunicode'))
            section.append( p )

            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( "" )
            section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "" )
    section.append( p )

    h2 = rtf.Paragraph( ss.ParagraphStyles.Heading2 )
    h2.append( "8. Atividades previstas para 2019".decode("utf-8").encode('rtfunicode') )
    section.append(h2)

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "8.1. Atividades de investigação no âmbito de projetos (nomeadamente projetos a submeter a financiamento):".decode('utf-8').encode('rtfunicode'))
    section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "" )
    section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "DOUTORADOS".decode('utf-8').encode('rtfunicode'))
    section.append( p )

    for doutor in form_doutor:
        if doutor["f8_1"]:
            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( doutor["nome"].replace("\n","").decode('utf-8').encode('rtfunicode'))
            section.append( p )

            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( doutor["f8_1"].replace("\n","").decode('utf-8').encode('rtfunicode'))
            section.append( p )

            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( "" )
            section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "" )
    section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "NÃO-DOUTORADOS".decode('utf-8').encode('rtfunicode'))
    section.append( p )

    for nao_dout in form_nao_doutor:
        if nao_dout["f8_1"]:
            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( nao_dout["nome"].replace("\n","").decode('utf-8').encode('rtfunicode'))
            section.append( p )

            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( nao_dout["f8_1"].replace("\n","").decode('utf-8').encode('rtfunicode'))
            section.append( p )

            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( "" )
            section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "" )
    section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "8.2. Publicações (indicar o título e se está em preparação, em fase de avaliação ou no prelo):".decode('utf-8').encode('rtfunicode'))
    section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "" )
    section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "DOUTORADOS".decode('utf-8').encode('rtfunicode'))
    section.append( p )

    for doutor in form_doutor:
        if doutor["f8_2"]:
            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( doutor["nome"].replace("\n","").decode('utf-8').encode('rtfunicode'))
            section.append( p )

            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( doutor["f8_2"].replace("\n","").decode('utf-8').encode('rtfunicode'))
            section.append( p )

            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( "" )
            section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "" )
    section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "NÃO-DOUTORADOS".decode('utf-8').encode('rtfunicode'))
    section.append( p )

    for nao_dout in form_nao_doutor:
        if nao_dout["f8_2"]:
            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( nao_dout["nome"].replace("\n","").decode('utf-8').encode('rtfunicode'))
            section.append( p )

            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( nao_dout["f8_2"].replace("\n","").decode('utf-8').encode('rtfunicode'))
            section.append( p )

            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( "" )
            section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "" )
    section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "8.3. Comunicações em colóquios (indicar URL do evento, título (provisório) da comunicação):".decode('utf-8').encode('rtfunicode'))
    section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "" )
    section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "DOUTORADOS".decode('utf-8').encode('rtfunicode'))
    section.append( p )

    for doutor in form_doutor:
        if doutor["f8_3"]:
            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( doutor["nome"].replace("\n","").decode('utf-8').encode('rtfunicode'))
            section.append( p )

            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( doutor["f8_3"].replace("\n","").decode('utf-8').encode('rtfunicode'))
            section.append( p )

            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( "" )
            section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "" )
    section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "NÃO-DOUTORADOS".decode('utf-8').encode('rtfunicode'))
    section.append( p )

    for nao_dout in form_nao_doutor:
        if nao_dout["f8_3"]:
            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( nao_dout["nome"].replace("\n","").decode('utf-8').encode('rtfunicode'))
            section.append( p )

            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( nao_dout["f8_3"].replace("\n","").decode('utf-8').encode('rtfunicode'))
            section.append( p )

            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( "" )
            section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "" )
    section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "8.4. Organização de colóquios e seminários (indicar tipo de evento, local, equipa organizadora, financiamento):".decode('utf-8').encode('rtfunicode'))
    section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "" )
    section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "DOUTORADOS".decode('utf-8').encode('rtfunicode'))
    section.append( p )

    for doutor in form_doutor:
        if doutor["f8-4"]:
            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( doutor["nome"].replace("\n","").decode('utf-8').encode('rtfunicode'))
            section.append( p )

            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( doutor["f8-4"].replace("\n","").decode('utf-8').encode('rtfunicode'))
            section.append( p )

            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( "" )
            section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "" )
    section.append( p )

    p = rtf.Paragraph( ss.ParagraphStyles.Normal )
    p.append( "NÃO-DOUTORADOS".decode('utf-8').encode('rtfunicode'))
    section.append( p )

    for nao_dout in form_nao_doutor:
        if nao_dout["f8-4"]:
            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( nao_dout["nome"].replace("\n","").decode('utf-8').encode('rtfunicode'))
            section.append( p )

            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( nao_dout["f8-4"].replace("\n","").decode('utf-8').encode('rtfunicode'))
            section.append( p )

            p = rtf.Paragraph( ss.ParagraphStyles.Normal )
            p.append( "" )
            section.append( p )

    response.headers['Content-Type']='text/rtf'
    return rtf.dumps(doc)

def ver_relatorio_html():
    if request.vars["linha"] == "linguagem":
        linha = "Ciências da Linguagem"
    elif request.vars["linha"] == "filosofia_cultura":
        linha = "Filosofia e Cultura"
    elif request.vars["linha"] == "literatura":
        linha = "Ciências da Literatura"
    elif request.vars["linha"] == "all":
        linha = "all"
    else:
        raise HTTP(500,'message')

    if linha != "all":
        form_doutor = relatorio = db( (db.relatorio.linha == linha) & (db.relatorio.grau == "Doutor") ).select(orderby=db.relatorio.nome)
        form_nao_doutor = relatorio = db( (db.relatorio.linha == linha) & (db.relatorio.grau != "Doutor") ).select(orderby=db.relatorio.nome)
    else:
        form_doutor = relatorio = db( db.relatorio.grau == "Doutor" ).select(orderby=db.relatorio.nome)
        form_nao_doutor = relatorio = db( db.relatorio.grau != "Doutor" ).select(orderby=db.relatorio.nome)

    html = DIV(
        H3("1. Projeto de investigação em curso"),
        H3("1. 1. Projeto de Mestrado, Doutoramento, Pós-Doutoramento (indicar título, data de início e supervisor):"),
        P(B("DOUTORADOS"), _style="text-decoration:underline;"),
        DIV( [SPAN(P(B(doutor["nome"])), PRE(doutor["f1_1"])) for doutor in form_doutor if doutor["f1_1"]]),
        BR(),
        P(B("NÃO-DOUTORADOS"), _style="text-decoration:underline;"),
        DIV( [SPAN(P(B(nao_dout["nome"])), PRE(nao_dout["f1_1"])) for nao_dout in form_nao_doutor if nao_dout["f1_1"]]),

        BR(),
        H3("1.2. Projeto(s) de investigação financiado(s) pela FCT ou outra entidade (indicar título, data de início e investigador responsável):"),
        P(B("DOUTORADOS"), _style="text-decoration:underline;"),
        DIV( [SPAN(P(B(doutor["nome"])), PRE(doutor["f1_2"])) for doutor in form_doutor if doutor["f1_2"]]),
        BR(),
        P(B("NÃO-DOUTORADOS"), _style="text-decoration:underline;"),
        DIV( [SPAN(P(B(nao_dout["nome"])), PRE(nao_dout["f1_2"])) for nao_dout in form_nao_doutor if nao_dout["f1_2"]]),

        BR(),

        H3("2. Internacionalização"),
        H3("2.1. Participação em Redes Internacionais e Europeias (indicar título, instituição responsável, financiamento e data de início e fim, se aplicável, e URL):"),
        P(B("DOUTORADOS"), _style="text-decoration:underline;"),
        DIV( [SPAN(P(B(doutor["nome"])), PRE(doutor["f2_1"])) for doutor in form_doutor if doutor["f2_1"]]),
        BR(),
        P(B("NÃO-DOUTORADOS"), _style="text-decoration:underline;"),
        DIV( [SPAN(P(B(nao_dout["nome"])), PRE(nao_dout["f2_1"])) for nao_dout in form_nao_doutor if nao_dout["f2_1"]]),

        BR(),

        H3("3. Publicações de caráter científico"),
        H3("Nota importante: As referências das publicações devem ser indicadas em formato APA, com indicação do URL (referente ao depósito em repositório / link da revista / DOI / indexação / e-Book)."),
        H3("3.1. Livros (indicar se é monografia, volume editado...):"),
        P(B("DOUTORADOS"), _style="text-decoration:underline;"),
        DIV( [SPAN(P(B(doutor["nome"])), PRE(doutor["f3_1"])) for doutor in form_doutor if doutor["f3_1"]]),
        BR(),
        P(B("NÃO-DOUTORADOS"), _style="text-decoration:underline;"),
        DIV( [SPAN(P(B(nao_dout["nome"])), PRE(nao_dout["f3_1"]))  for nao_dout in form_nao_doutor if nao_dout["f3_1"]]),

        BR(),

        H3("3.2. Capítulos de livros (indicar se for peer review)"),
        H3("3.2.1. Capítulos de livros nacionais:"),
        P(B("DOUTORADOS"), _style="text-decoration:underline;"),
        DIV( [SPAN(P(B(doutor["nome"])), PRE(doutor["f3_2_1"])) for doutor in form_doutor if doutor["f3_2_1"]]),
        BR(),
        P(B("NÃO-DOUTORADOS"), _style="text-decoration:underline;"),
        DIV( [SPAN(P(B(nao_dout["nome"])), PRE(nao_dout["f3_2_1"])) for nao_dout in form_nao_doutor if nao_dout["f3_2_1"]]),

        BR(),

        H3("3.2.2. Capítulos de livros internacionais:"),
        P(B("DOUTORADOS"), _style="text-decoration:underline;"),
        DIV( [SPAN(P(B(doutor["nome"])), PRE(doutor["f3_2_2"])) for doutor in form_doutor if doutor["f3_2_2"]]),
        BR(),
        P(B("NÃO-DOUTORADOS"), _style="text-decoration:underline;"),
        DIV( [SPAN(P(B(nao_dout["nome"])), PRE(nao_dout["f3_2_2"])) for nao_dout in form_nao_doutor if nao_dout["f3_2_2"]]),

        BR(),

        H3("3.3. Artigos em livros de atas:"),
        P(B("DOUTORADOS"), _style="text-decoration:underline;"),
        DIV( [SPAN(P(B(doutor["nome"])), PRE(doutor["f3_3"])) for doutor in form_doutor if doutor["f3_3"]]),
        BR(),
        P(B("NÃO-DOUTORADOS"), _style="text-decoration:underline;"),
        DIV( [SPAN(P(B(nao_dout["nome"])), PRE(nao_dout["f3_3"])) for nao_dout in form_nao_doutor if nao_dout["f3_3"]]),

        BR(),

        H3("3.4. Publicações em Revistas (indicar se for peer review)"),
        H3("3.4.1. Artigos em revistas nacionais:"),
        P(B("DOUTORADOS"), _style="text-decoration:underline;"),
        DIV( [SPAN(P(B(doutor["nome"])), PRE(doutor["f3_4_1"])) for doutor in form_doutor if doutor["f3_4_1"]]),
        BR(),
        P(B("NÃO-DOUTORADOS"), _style="text-decoration:underline;"),
        DIV( [SPAN(P(B(nao_dout["nome"])), PRE(nao_dout["f3_4_1"])) for nao_dout in form_nao_doutor if nao_dout["f3_4_1"]]),

        BR(),

        H3("3.4.2 Artigos em revistas internacionais:"),
        P(B("DOUTORADOS"), _style="text-decoration:underline;"),
        DIV( [SPAN(P(B(doutor["nome"])), PRE(doutor["f3_4_2"])) for doutor in form_doutor if doutor["f3_4_2"]]),
        BR(),
        P(B("NÃO-DOUTORADOS"), _style="text-decoration:underline;"),
        DIV( [SPAN(P(B(nao_dout["nome"])), PRE(nao_dout["f3_4_2"])) for nao_dout in form_nao_doutor if nao_dout["f3_4_2"]]),

        BR(),

        H3("3.5.  Outras publicações (não indicar publicações no prelo):"),
        P(B("DOUTORADOS"), _style="text-decoration:underline;"),
        DIV( [SPAN(P(B(doutor["nome"])), PRE(doutor["f3_5"])) for doutor in form_doutor if doutor["f3_5"]]),
        BR(),
        P(B("NÃO-DOUTORADOS"), _style="text-decoration:underline;"),
        DIV( [SPAN(P(B(nao_dout["nome"])), PRE(nao_dout["f3_5"])) for nao_dout in form_nao_doutor if nao_dout["f3_5"]]),

        BR(),

        H3("4. Comunicações"),
        H3("4.1. Encontros científicos nacionais (indicar se por convite ou submissão de resumo):"),
        P(B("DOUTORADOS"), _style="text-decoration:underline;"),
        DIV( [SPAN(P(B(doutor["nome"])), PRE(doutor["f4_1"])) for doutor in form_doutor if doutor["f4_1"]]),
        BR(),
        P(B("NÃO-DOUTORADOS"), _style="text-decoration:underline;"),
        DIV( [SPAN(P(B(nao_dout["nome"])), PRE(nao_dout["f4_1"])) for nao_dout in form_nao_doutor if nao_dout["f4_1"]]),

        BR(),

        H3("4.2. Encontros científicos internacionais (indicar se por convite ou submissão de resumo):"),
        P(B("DOUTORADOS"), _style="text-decoration:underline;"),
        DIV( [SPAN(P(B(doutor["nome"])), PRE(doutor["f4_2"])) for doutor in form_doutor if doutor["f4_2"]]),
        BR(),
        P(B("NÃO-DOUTORADOS"), _style="text-decoration:underline;"),
        DIV( [SPAN(P(B(nao_dout["nome"])), PRE(nao_dout["f4_2"])) for nao_dout in form_nao_doutor if nao_dout["f4_2"]]),

        BR(),

        H3("5. Organização de Colóquios ou Seminários:"),
        H3("Indicar título, data, local, o URL do evento."),
        P(B("DOUTORADOS"), _style="text-decoration:underline;"),
        DIV( [SPAN(P(B(doutor["nome"])), PRE(doutor["f5"])) for doutor in form_doutor if doutor["f5"]]),
        BR(),
        P(B("NÃO-DOUTORADOS"), _style="text-decoration:underline;"),
        DIV( [SPAN(P(B(nao_dout["nome"])), PRE(nao_dout["f5"])) for nao_dout in form_nao_doutor if nao_dout["f5"]]),

        BR(),

        H3("6. Orientação de teses concluídas em 2018"),
        H3("6.1. Doutoramentos (título, data de defesa e nome do candidato):"),
        P(B("DOUTORADOS"), _style="text-decoration:underline;"),
        DIV( [SPAN(P(B(doutor["nome"])), PRE(doutor["f6_1"])) for doutor in form_doutor if doutor["f6_1"]]),
        BR(),
        P(B("NÃO-DOUTORADOS"), _style="text-decoration:underline;"),
        DIV( [SPAN(P(B(nao_dout["nome"])), PRE(nao_dout["f6_1"])) for nao_dout in form_nao_doutor if nao_dout["f6_1"]]),

        BR(),

        H3("6.2. Mestrados (título, data de defesa e nome do candidato):"),
        P(B("DOUTORADOS"), _style="text-decoration:underline;"),
        DIV( [SPAN(P(B(doutor["nome"])), PRE(doutor["f6_2"])) for doutor in form_doutor if doutor["f6_2"]]),
        BR(),
        P(B("NÃO-DOUTORADOS"), _style="text-decoration:underline;"),
        DIV( [SPAN(P(B(nao_dout["nome"])), PRE(nao_dout["f6_2"])) for nao_dout in form_nao_doutor if nao_dout["f6_2"]]),

        BR(),

        H3("7. Participação em equipas editoriais e comissões científicas de revistas (indicar cargo, revista e URL)"),
        P(B("DOUTORADOS"), _style="text-decoration:underline;"),
        DIV( [SPAN(P(B(doutor["nome"])), PRE(doutor["f7"])) for doutor in form_doutor if doutor["f7"]]),
        BR(),
        P(B("NÃO-DOUTORADOS"), _style="text-decoration:underline;"),
        DIV( [SPAN(P(B(nao_dout["nome"])), PRE(nao_dout["f7"])) for nao_dout in form_nao_doutor if nao_dout["f7"]]),

        BR(),

        H3("7. Atividades previstas para 2019"),
        H3("8.1. Atividades de investigação no âmbito de projetos (nomeadamente projetos a submeter a financiamento):"),
        P(B("DOUTORADOS"), _style="text-decoration:underline;"),
        DIV( [SPAN(P(B(doutor["nome"])), PRE(doutor["f8_1"])) for doutor in form_doutor if doutor["f8_1"]]),
        BR(),
        P(B("NÃO-DOUTORADOS"), _style="text-decoration:underline;"),
        DIV( [SPAN(P(B(nao_dout["nome"])), PRE(nao_dout["f8_1"])) for nao_dout in form_nao_doutor if nao_dout["f8_1"]]),

        BR(),

        H3("8.2. Publicações (indicar o título e se está em preparação, em fase de avaliação ou no prelo):"),
        P(B("DOUTORADOS"), _style="text-decoration:underline;"),
        DIV( [SPAN(P(B(doutor["nome"])), PRE(doutor["f8_2"])) for doutor in form_doutor if doutor["f8_2"]]),
        BR(),
        P(B("NÃO-DOUTORADOS"), _style="text-decoration:underline;"),
        DIV( [SPAN(P(B(nao_dout["nome"])), PRE(nao_dout["f8_2"])) for nao_dout in form_nao_doutor if nao_dout["f8_2"]]),

        BR(),

        H3("8.3. Comunicações em colóquios (indicar URL do evento, título (provisório) da comunicação):"),
        P(B("DOUTORADOS"), _style="text-decoration:underline;"),
        DIV( [SPAN(P(B(doutor["nome"])), PRE(doutor["f8_3"])) for doutor in form_doutor if doutor["f8_3"]]),
        BR(),
        P(B("NÃO-DOUTORADOS"), _style="text-decoration:underline;"),
        DIV( [SPAN(P(B(nao_dout["nome"])), PRE(nao_dout["f8_3"])) for nao_dout in form_nao_doutor if nao_dout["f8_3"]]),

        BR(),

        H3("8.4. Organização de colóquios e seminários (indicar tipo de evento, local, equipa organizadora, financiamento):"),
        P(B("DOUTORADOS"), _style="text-decoration:underline;"),
        DIV( [SPAN(P(B(doutor["nome"])), PRE(doutor["f8-4"])) for doutor in form_doutor if doutor["f8-4"]]),
        BR(),
        P(B("NÃO-DOUTORADOS"), _style="text-decoration:underline;"),
        DIV( [SPAN(P(B(nao_dout["nome"])), PRE(nao_dout["f8-4"])) for nao_dout in form_nao_doutor if nao_dout["f8-4"]]),
        )
    return dict(html=html, linha=linha)

def user():
    """
    exposes:
    http://..../[app]/default/user/login
    http://..../[app]/default/user/logout
    http://..../[app]/default/user/register
    http://..../[app]/default/user/profile
    http://..../[app]/default/user/retrieve_password
    http://..../[app]/default/user/change_password
    use @auth.requires_login()
        @auth.requires_membership('group name')
        @auth.requires_permission('read','table name',record_id)
    to decorate functions that need access control
    """
    return dict(form=auth())


def download():
    """
    allows downloading of uploaded files
    http://..../[app]/default/download/[filename]
    """
    return response.download(request, db)


def call():
    """
    exposes services. for example:
    http://..../[app]/default/call/jsonrpc
    decorate with @services.jsonrpc the functions to expose
    supports xml, json, xmlrpc, jsonrpc, amfrpc, rss, csv
    """
    return service()


@auth.requires_signature()
def data():
    """
    http://..../[app]/default/data/tables
    http://..../[app]/default/data/create/[table]
    http://..../[app]/default/data/read/[table]/[id]
    http://..../[app]/default/data/update/[table]/[id]
    http://..../[app]/default/data/delete/[table]/[id]
    http://..../[app]/default/data/select/[table]
    http://..../[app]/default/data/search/[table]
    but URLs must be signed, i.e. linked with
      A('table',_href=URL('data/tables',user_signature=True))
    or with the signed load operator
      LOAD('default','data.load',args='tables',ajax=True,user_signature=True)
    """
    return dict(form=crud())
